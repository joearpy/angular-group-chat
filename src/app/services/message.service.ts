import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  private baseURL: string = environment.apiBaseUrl;

  constructor(private http: HttpClient, private cookieservice: CookieService) {}

  public getAllMessages() {
    return this.http.get<any>(this.baseURL + 'message/get_all_messages.php');
  }

  public saveMessage(message) {
    let userName = this.cookieservice.get('authenticated_user');
    return this.http.post<any>(this.baseURL + 'message/save_message.php', {
      message,
      userName,
    });
  }
}
