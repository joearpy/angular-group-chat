import { Injectable } from '@angular/core';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private baseURL: string = environment.apiBaseUrl;

  constructor(private http: HttpClient, private cookieservice: CookieService) {}

  setLoggedIn(value: string) {
    this.cookieservice.set('is_auth_user', value);
  }

  isLoggedIn() {
    return this.cookieservice.get('is_auth_user');
  }

  getUserDetails(username: string) {
    return this.http.post<any>(this.baseURL + 'user/auth.php', {
      username,
    });
  }

  setUserToCookie(username: string) {
    this.cookieservice.set('authenticated_user', username);
  }

  logout() {
    this.cookieservice.deleteAll();
    return this.http.get<any>(this.baseURL + 'user/logout.php');
  }
}
