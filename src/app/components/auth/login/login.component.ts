import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private cookieservice: CookieService) {
  }

  ngOnInit(): void {}

  public doLogin(e: Event, nickname: string) {
    e.preventDefault();
    this.userService.getUserDetails(nickname).subscribe(
      (response) => {
        if (response.success) {
          this.userService.setLoggedIn('true');
          this.userService.setUserToCookie(response.authenticated_user);
          this.router.navigate(['chat']);
        } else {
          alert(response.secret);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
