import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  public allMessages = [];

  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getAllMessages();
  }

  public doLogout(e) {
    e.preventDefault();

    this.userService.logout().subscribe(
      (response) => {
        if (response.success) {
          this.router.navigate(['login']);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  public getAllMessages() {
    this.messageService.getAllMessages().subscribe(
      (response) => {
        this.allMessages = response;
        console.log(this.allMessages);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  public saveMessage (event, message) {
    event.preventDefault();
    this.messageService.saveMessage(message).subscribe(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
